library ieee;
use ieee.std_logic_1164.all;

entity div_freq is
	generic(
		fin_cont :	integer := 12e6
	);
	port(
		clk :	in std_logic;
		dclk :	out	std_logic
	);
end div_freq;

architecture behavioral of div_freq is

	signal aux : std_logic := '0';

begin

	process (clk)
		variable cont : integer range 0 to fin_cont := 0;
	begin
		if clk'event and clk = '1' then --Rising_edge(clk)
			if cont = fin_cont then
				aux <= not aux;
				cont := 0;
			else
				cont := cont + 1;
			end if;
		end if;
	end process;
	
	dclk <= aux;
	
end architecture;