library ieee;
use ieee.std_logic_1164.all;

entity Div is 
	port(
		clk :		in	std_logic;
		dclk:		out	std_logic_vector(3 downto 0)
	);
end Div;


architecture behavioral	of div is

begin

	U0:	work.div_freq 
		generic map (fin_cont => 50_000_000) 
		port map (clk => clk, dclk => dclk(0));
	U1:	work.div_freq 
		generic map (fin_cont => 25_000_000) 
		port map (clk => clk, dclk => dclk(1));
	U2:	work.div_freq 
		generic map (fin_cont => 12_500_000) 
		port map (clk => clk, dclk => dclk(2));
	U3:	work.div_freq 
		generic map (fin_cont => 06_250_000) 
		port map (clk => clk, dclk => dclk(3));

end architecture;